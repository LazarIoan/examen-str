public class ExecutionThread1 extends Thread {
    int min;
    int max;

    public ExecutionThread1(int min, int max, Integer a){
        this.min=min;
        this.max=max;
    }

    public void run(){
        System.out.println(this.getName() + " - STATE 1");
        synchronized (ExecutionThread1.class) {


            int k = (int) Math.round(Math.random() * (max - min) + min);
            for (int i = 0; i < k * 100000; i++) {
                i++;
                i--;
            }
            System.out.println(this.getName() + " - STATE 2");
        }
        System.out.println(this.getName() + " - STATE 3");
    }


}
